This repository is a seed for building Angular projects. It includes a structure of folders, modules separated and a core module where whole business logic is stored. The pattern of folders and structure does not need to be used as a rule but as an example. However, it is recommended to follow it.

*It is in construction!*

# Angular version

Version 8x

# Project structure

## Config

This folder should have the configuration set for you app/server and constants. In my case I usually set the sensitive data in environment files and then load to my app constants.

## Core Module

This module is where whole business logic is stored.
Here we can find out the app model, services, api request, interceptors, guards, etc.

Also here will be include whole singleton services and an auth service template, using local storage.

### mocks

This folder is essentially useful for testing, here we can store dummy data to develop and test until the backend is ready or simply without using it.

### http

Here you should store all api requests and their respective abstraction for error handling, which will be supported by interceptors.

### access

In this folder you will find whole auth logic for the app access.

### services

Here you could handle as an aside way whole services that the components need for manipulate view with data provide.

### model

Here you can define the models of your app. It is recommended to define only one class/interface for each file. Also you can create a tree that contains subfolders as /interfaces, /dto separately.

## Modules

Inside this folder, we add the views and pages of the app (understood view/pages as a set of components), each one with his own module, inner routes, own pipes, directives, etc.
The idea of doing this is use lazy loading and charge each module with his own whole dependencies without using other inner app modules, excepto for core logic and any shared component.

## Shared Module

Here we will contain pipes, services, directives, and also components that can be used by the whole app, like buttons, custom inputs, etc.
Whoever that import this module can reuse the items it has.

Note: This module SHOULD NOT had ANY dependency from the rest of the app (for example import other modules from the inner app)


...*
