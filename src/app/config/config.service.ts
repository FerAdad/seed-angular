import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ConfigService {
	/* En este archivo se guardan las constantes de la app */

	static readonly baseUrl = environment.apiUrl;
	static readonly secret = environment.secret;
}
