import { Injectable } from '@angular/core';
import { CredencialesService, Credencial } from './credenciales.service';
import { Observable, of, Subject } from 'rxjs';

export interface LoginContext {
  username: string;
  password: string;
}

/**
 * Provee una base para el flujo de autenticación
 * Los metodos login/logout pueden remplazarse por la implementación correcta
 */
@Injectable()
export class AuthService {
  constructor(private credencialesServicio: CredencialesService) {}

  login(context: LoginContext): Observable<Credencial> {
    return of(null);
  }

  logout(): Observable<boolean> {
    return of(null);
  }
}
