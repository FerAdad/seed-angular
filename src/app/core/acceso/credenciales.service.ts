import { Injectable } from '@angular/core';
import { Credencial } from '../model';

const credentialsKey = 'credenciales';

/**
 * Provee almacenamiento para las credenciales de autenticación de usuario
 * La interfaz de Credencial debe ser remplazada con su correcta implementación
 */
@Injectable()
export class CredencialesService {
  private _credenciales: Credencial | null = null;

  constructor() {
    const credencialesGuardadas = localStorage.getItem(credentialsKey);

    if (credencialesGuardadas) {
      this._credenciales = JSON.parse(credencialesGuardadas);
    }
  }

  estaAutenticado(): boolean {
    return !!this.credenciales;
  }

  get credenciales(): Credencial | null {
    return this._credenciales;
  }

  establecerCredenciales(credenciales?: Credencial) {
    this._credenciales = credenciales || null;

    if (credenciales) {
      localStorage.setItem(credentialsKey, JSON.stringify(credenciales));
    } else {
      localStorage.removeItem(credentialsKey);
    }
  }
}
