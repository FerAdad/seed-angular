import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { CredencialesService } from '../acceso/credenciales.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    public router: Router,
    private credencialesServicio: CredencialesService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this.credencialesServicio.estaAutenticado()) {
      return true;
    }

    this.router.navigate(['/login'], {
      queryParams: { redirect: state.url },
      replaceUrl: true
    });
    return false;
  }
}
