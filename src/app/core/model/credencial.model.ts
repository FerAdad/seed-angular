export interface Credencial {
  username: string;
  token: string;
}
