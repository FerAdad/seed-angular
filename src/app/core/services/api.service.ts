import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { environment } from '../../../environments/environment.example';

interface HttpOptions {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  observe?: 'body';
  reportProgress?: boolean;
  withCredentials?: boolean;
  responseType?: 'json';

}

@Injectable()
export class ApiService {
  apiUrl = '';

  constructor(private http: HttpClient) { }

  get(path: string, options?: HttpOptions): Observable<any> {
    const url = `${this.apiUrl}${path}`;

    return this.http
      .get(url, options)
      .pipe(map(res => {
        this.onSuccess(res);
        return res;
      }), catchError(this.onCatch));
  }

  put(
    path: string,
    body: object = {},
    options?: HttpOptions
  ): Observable<any> {
    const url = `${this.apiUrl}${path}`;

    return this.http
      .put(url, body, options)
      .pipe(map(res => {
        this.onSuccess(res);
        return res;
      }), catchError(this.onCatch));
  }

  post(
    path: string,
    body: object = {},
    options?: HttpOptions
  ): Observable<any> {
    const url = `${this.apiUrl}${path}`;

    return this.http
      .post(url, body, options)
      .pipe(map(res => {
        this.onSuccess(res);
        return res;
      }), catchError(this.onCatch));
  }

  delete(path, options?: HttpOptions): Observable<any> {
    const url = `${this.apiUrl}${path}`;

    return this.http
      .delete(url, options)
      .pipe(map(res => {
        this.onSuccess(res);
        return res;
      }), catchError(this.onCatch));
  }

  private onCatch(error: any) {
    return throwError(error.error);
  }

  private onSuccess(res) {
    if (environment.production) {
      if (res.success) {
        console.log('Request successfully: ', res);
      } else {
        console.log('Request not success: ', res);
      }
    }
  }
}
