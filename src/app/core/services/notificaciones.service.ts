import { Injectable } from '@angular/core';

@Injectable()
export class NotificacionesService {
    constructor() {}

    default(message: string) {}

    info(message: string) {}

    success(message: string) {}

    warn(message: string) {}

    error(message: string) {}

    private display(message: string) {}
}
