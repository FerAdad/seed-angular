import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../../shared/components/modal/modal.component';
import { AuthService, LoginContext } from '../../core/acceso/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalComponent;

  loginContext: LoginContext;

  constructor(private authService: AuthService) {}

  ngOnInit() {}

  login() {}
}
